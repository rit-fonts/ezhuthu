# Copyright 2019-2024 Rajeesh KV <rajeeshknambiar@gmail.com>, Hussain KH <hussain.rachana@gmail.com>,
# Rachana Institute of Typography <rachana.org.in>
# This file is licensed under OFL 1.1

# Malayalam OpenType feature program for comprehensive orthography in traditional script

languagesystem mlym dflt;


# Lookups

include (ml-gsub-lookups.fea);

# mlym specific lookups

lookup pstf_signs_mlym {
    sub r3 xx  by r4;
    sub y1 xx  by y2;
    sub v1 xx  by v2;
} pstf_signs_mlym;

lookup blwf_signs_la_mlym {
    sub l3 xx  by l4;
} blwf_signs_la_mlym;

lookup blws_signs_la_mlym {
    sub k1 l4  by k1l3;
    sub k3 l4  by k3l3;
    sub th1 l4  by th1l3;
    sub p1 l4  by p1l3;
    sub p2 l4  by p2l3;
    sub p3 l4  by p3l3;
    sub m1 l4  by m1l3;
    sub l3 l4  by l3l3;
    sub v1 l4  by v1l3;
    sub z1 l4  by z1l3;
    sub s1 l4  by s1l3;
    sub h1 l4  by h1l3;
} blws_signs_la_mlym;

lookup pres_signs_ra_mlym {
    sub k1k1 r4  by k1k1r3;     # Needed for Windows XP shaper
    sub k1k1 xx r3  by k1k1r3;  # Needed for old Pango 1.6.0
    sub k1t1 r4  by k1t1r3;
    sub k1t1 xx r3  by k1t1r3;
    sub k1th1 r4  by k1th1r3;
    sub k1th1 xx r3  by k1th1r3;
    sub k1 r4  by k1r3;
    sub k1 xx r3  by k1r3;
    sub k3th3th4 r4  by k3th3th4r3;
    sub k3th3th4 xx r3  by k3th3th4r3;
    sub k3 r4  by k3r3;
    sub k3 xx r3  by k3r3;
    sub k4 r4  by k4r3;
    sub k4 xx r3  by k4r3;
    sub ch1ch2 r4  by ch1ch2r3;
    sub ch1ch2 xx r3  by ch1ch2r3;
    sub ch2 r4  by ch2r3;
    sub ch2 xx r3  by ch2r3;
    sub ch3 r4  by ch3r3;
    sub ch3 xx r3  by ch3r3;
    sub ch4 r4  by ch4r3;
    sub ch4 xx r3  by ch4r3;
    sub t1 r4  by t1r3;
    sub t1 xx r3  by t1r3;
    sub t3 r4  by t3r3;
    sub t3 xx r3  by t3r3;
    sub t4 r4  by t4r3;
    sub t4 xx r3  by t4r3;
    sub nht1 r4  by nht1r3;
    sub nht1 xx r3  by nht1r3;
    sub nht3 r4  by nht3r3;
    sub nht3 xx r3  by nht3r3;
    sub th1th1 r4  by th1th1r3;
    sub th1th1 xx r3  by th1th1r3;
    sub th1 r4  by th1r3;
    sub th1 xx r3  by th1r3;
    sub th1s1 r4  by th1s1r3;
    sub th1s1 xx r3  by th1s1r3;
    sub th3th4 r4  by th3th4r3;
    sub th3th4 xx r3  by th3th4r3;
    sub th3 r4  by th3r3;
    sub th3 xx r3  by th3r3;
    sub th4 r4  by th4r3;
    sub th4 xx r3  by th4r3;
    sub n1th1 r4  by n1th1r3;
    sub n1th1 xx r3  by n1th1r3;
    sub n1th3 r4  by n1th3r3;
    sub n1th3 xx r3  by n1th3r3;
    sub n1th4 r4  by n1th4r3;
    sub n1th4 xx r3  by n1th4r3;
    sub n1n1 r4  by n1n1r3;
    sub n1n1 xx r3  by n1n1r3;
    sub n1m1 r4  by n1m1r3;
    sub n1m1 xx r3  by n1m1r3;
    sub n1 r4  by n1r3;
    sub n1 xx r3  by n1r3;
    sub p1 r4  by p1r3;
    sub p1 xx r3  by p1r3;
    sub p1s1 r4  by p1s1r3;
    sub p1s1 xx r3  by p1s1r3;
    sub p2 r4  by p2r3;
    sub p2 xx r3  by p2r3;
    sub p3 r4  by p3r3;
    sub p3 xx r3  by p3r3;
    sub p4 r4  by p4r3;
    sub p4 xx r3  by p4r3;
    sub m1p1 r4  by m1p1r3;
    sub m1p1 xx r3  by m1p1r3;
    sub m1 r4  by m1r3;
    sub m1 xx r3  by m1r3;
    sub l3p1 r4  by l3p1r3;
    sub l3p1 xx r3  by l3p1r3;
    sub v1 r4  by v1r3;
    sub v1 xx r3  by v1r3;
    sub z1 r4  by z1r3;
    sub z1 xx r3  by z1r3;
    sub z1z1 r4  by z1z1r3;
    sub z1z1 xx r3  by z1z1r3;
    sub shk1 r4  by shk1r3;
    sub shk1 xx r3  by shk1r3;
    sub sht1 r4  by sht1r3;
    sub sht1 xx r3  by sht1r3;
    sub shp1 r4  by shp1r3;
    sub shp1 xx r3  by shp1r3;
    sub sh r4  by shr3;
    sub sh xx r3  by shr3;
    sub s1k1 r4  by s1k1r3;
    sub s1k1 xx r3  by s1k1r3;
    sub s1t1 r4  by s1t1r3;
    sub s1t1 xx r3  by s1t1r3;
    sub s1th1 r4  by s1th1r3;
    sub s1th1 xx r3  by s1th1r3;
    sub s1p1 r4  by s1p1r3;
    sub s1p1 xx r3  by s1p1r3;
    sub s1 r4  by s1r3;
    sub s1 xx r3  by s1r3;
    sub s1s1 r4  by s1s1r3;
    sub s1rhrh r4  by s1rhrhr3;
    sub s1rhrh xx r3  by s1rhrhr3;  # rhrh xx r3 isn't reordered by Windows XP :-)
    sub h1 r4  by h1r3;
    sub h1 xx r3  by h1r3;
} pres_signs_ra_mlym;

feature akhn {
    script mlym;
        lookup akhn_cil;                    #Chillu letters
        lookup akhn_nosigns_double_cons;    #Conjuncts with double consonants
        lookup akhn_nosigns_3_cons;         #Conjuncts with 3 consonants sans 'u1/u2/r1' signs
        lookup akhn_nosigns_2_cons;         #Conjuncts with 2 consonants sans 'u1/u2/r1' signs
        lookup akhn_nosigns_other_cons;     #Conjuncts with ya/ra/la/za/sa/ha/lha/zha/rha
} akhn;

feature pstf {
    script mlym;
        lookup pstf_signs_mlym;
} pstf;

feature blwf {
    script mlym;
        lookup blwf_signs_la_mlym;
} blwf;

feature pres {
    script mlym;
        lookup pres_signs_ra_mlym;
} pres;

feature blws {
    script mlym;
        lookup blws_signs_la_mlym;
} blws;

feature psts {
    script mlym;
      lookup psts_signs_u_uu;
} psts;
